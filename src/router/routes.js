
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') }
    ]
  },
  {
    path: '/catalogo',
    component: () => import('src/layouts/catalogo/main.vue'),
    children: [
      { path: ':username', component: () => import('pages/catalogo/index.vue') }
    ]
  },
  {
    path: '/dashboard',
    component: () => import('layouts/dashboard/dashboard.vue'),
    children: [
      {
        path: 'main',
        component: () => import('pages/dashboard/main'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'clients',
        component: () => import('pages/dashboard/clients/index'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'client/filter',
        component: () => import('pages/dashboard/clients/filter'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'products',
        component: () => import('pages/dashboard/products/index'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'products/filter',
        component: () => import('pages/dashboard/products/filter'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'orders',
        component: () => import('pages/dashboard/orders/index'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'orders/filter',
        component: () => import('pages/dashboard/orders/filter'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'history',
        component: () => import('pages/dashboard/history/index'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'proveedores',
        component: () => import('pages/dashboard/proveedor/index'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'category',
        component: () => import('pages/dashboard/category/index'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'subcategory',
        component: () => import('pages/dashboard/subcategory/index'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'proveedor/filter',
        component: () => import('pages/dashboard/proveedor/filter'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'themes',
        component: () => import('pages/dashboard/themes/index'),
        meta: {
          requiresAuth: true
        }
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
