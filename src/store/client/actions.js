import { addClient, getClients, deleteClients, getDistriProd, pdf, updateClients } from './../../api/clientCall'

export function add ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return addClient(data, (response) => {
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function get ({ commit }) {
	return new Promise((resolve, reject) => {
		return getClients((response) => {
			commit('setClients', response.data.success)
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function deleteClient ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return deleteClients(data, (response) => {
			commit('setClients', response.data.success)
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function updateClient ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return updateClients(data, (response) => {
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function getDistriProds ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return getDistriProd(data, (response) => {
			commit('setUserProds', response.data.success.orders)
			commit('setFilterClient', response.data.success.client)
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function getPdfs ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return pdf(data, (response) => {
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function setFilter ({ commit }, data) {
	return new Promise((resolve, reject) => {
		commit('setFilterClient', data)
		resolve(data)
	})
}
