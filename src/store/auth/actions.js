import { registerUser, loginUser, getRestaurant, recoveryChangePassword, changePasssWord, changeImg } from './../../api/apiCall'

export function register ({ commit }, user) {
	return new Promise((resolve, reject) => {
		return registerUser(user, (response) => {
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function login ({ commit }, login) {
	return new Promise ((resolve, reject) => {
		localStorage.removeItem('userId')
  		localStorage.removeItem('user')
		return loginUser(login, (response) => {
			if (response.data.success) {
			  localStorage.setItem('user', JSON.stringify(response.data.success))
			  localStorage.setItem('userid', response.data.success.id)
			  commit('loginSuccess', response.data.success)
			}
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function logout ({ commit }) {
	return new Promise ((resolve, reject) => {
		localStorage.removeItem('userId')
		localStorage.removeItem('user')
		commit('logout')
		resolve()
	})
}

export function changeBrands ({ commit }, data) {
	return new Promise ((resolve, reject) => {
		localStorage.removeItem('userId')
  		localStorage.removeItem('user')
		return changeImg(data, (response) => {
			if (response.data.success) {
			  localStorage.setItem('user', JSON.stringify(response.data.success))
			  localStorage.setItem('userid', response.data.success.id)
			  commit('loginSuccess', response.data.success)
			  resolve(response.data.success)
			}
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}
