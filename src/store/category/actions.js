import { addCategory, getCategory, deleteCategory, getTheme, addTheme, deleteTheme, editCategory, editThemes } from './../../api/category'

export function add ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return addCategory(data, (response) => {
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function edit ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return editCategory(data, (response) => {
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function addThemes ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return addTheme(data, (response) => {
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function editTheme ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return editThemes(data, (response) => {
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}


export function get ({ commit }) {
	return new Promise((resolve, reject) => {
		return getCategory((response) => {
			commit('setCategories', response.data.success)
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function deletes ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return deleteCategory(data, (response) => {
			commit('setCategories', response.data.success)
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function deleteThemes ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return deleteTheme(data, (response) => {
			commit('setThmes', response.data.success)
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function getThemes ({ commit }) {
	return new Promise((resolve, reject) => {
		return getTheme((response) => {
			commit('setThemes', response.data.success)
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}
