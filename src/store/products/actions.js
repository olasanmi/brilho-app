import { addProducts, getProducts, deleteProducts, updateProduct, getProductUser, deleteProductOrder, toggleBagsState, ediImgs } from './../../api/productsCall'

export function add ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return addProducts(data, (response) => {
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function ediImg ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return ediImgs(data, (response) => {
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function deleteOrderProduct ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return deleteProductOrder(data, (response) => {
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function toggleBags ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return toggleBagsState(data, (response) => {
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function get ({ commit }) {
	return new Promise((resolve, reject) => {
		return getProducts((response) => {
			commit('setProducts', response.data.success)
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function getProductByUser ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return getProductUser(data, (response) => {
			commit('setProducts', response.data.success)
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function deleteProduct ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return deleteProducts(data, (response) => {
			commit('setProducts', response.data.success)
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function updateProducts ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return updateProduct(data, (response) => {
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function setFilter ({ commit }, data) {
	return new Promise((resolve, reject) => {
		commit('setFilterProduct', data)
		resolve(data)
	})
}
