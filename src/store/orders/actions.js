import { saveOrder, getOrders, updateIsActives, deleteOrder, updateOrders, getToday, getTodaySell, getDomicilio } from './../../api/orderCall'

export function saveOrders ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return saveOrder(data, (response) => {
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function updateIsActive ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return updateIsActives(data, (response) => {
			commit('setOrder', response.data.success)
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function updateOrder ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return updateOrders(data, (response) => {
			commit('setOrder', response.data.success)
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function get ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return getOrders(data, (response) => {
			commit('setOrders', response.data.success)
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function getTodaySells ({ commit }) {
	return new Promise((resolve, reject) => {
		return getToday((response) => {
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function getTodayAll ({ commit }) {
	return new Promise((resolve, reject) => {
		return getTodaySell((response) => {
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function getDomicilios ({ commit }) {
	return new Promise((resolve, reject) => {
		return getDomicilio((response) => {
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function setFilter ({ commit }, data) {
	return new Promise((resolve, reject) => {
		commit('setOrder', data)
		resolve(data)
	})
}

export function deleteData ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return deleteOrder(data, (response) => {
			commit('setOrders', response.data.success)
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}
