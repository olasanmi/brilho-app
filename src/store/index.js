import Vue from 'vue'
import Vuex from 'vuex'

import auth from './auth'
import client from './client'
import products from './products'
import orders from './orders'
import proveedor from './proveedor'
import category from './category'
import subcategory from './subcategory'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      auth,
      client,
      products,
      orders,
      proveedor,
      category,
      subcategory
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })

  return Store
}
