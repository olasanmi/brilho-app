import { addCategory, getCategory, deleteCategory, editCategory } from './../../api/subcategory'

export function add ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return addCategory(data, (response) => {
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function edit ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return editCategory(data, (response) => {
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function get ({ commit }) {
	return new Promise((resolve, reject) => {
		return getCategory((response) => {
			commit('setSubcategory', response.data.success)
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function deletes ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return deleteCategory(data, (response) => {
			commit('setSubcategory', response.data.success)
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}
