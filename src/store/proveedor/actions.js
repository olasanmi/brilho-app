import { addProveedor, getProveedor, addFactura, deleteProveedors, toggleStatuss } from './../../api/proveedorCall'

export function add ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return addProveedor(data, (response) => {
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function factura ({ commit }, data) {
	return new Promise((resolve, reject) => {
		return addFactura(data, (response) => {
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function get ({ commit }) {
	return new Promise((resolve, reject) => {
		return getProveedor((response) => {
			commit('setProveedor', response.data.success)
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function setFilter ({ commit }, data) {
	return new Promise((resolve, reject) => {
		commit('setFilterClient', data)
		resolve(data)
	})
}

export function deleteProveedor({ commit }, data) {
	return new Promise((resolve, reject) => {
		return deleteProveedors(data, (response) => {
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function toggleStatus({ commit }, data) {
	return new Promise((resolve, reject) => {
		return toggleStatuss(data, (response) => {
			commit('setFilterClient', response.data.success)
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}
