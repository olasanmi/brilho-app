import axios from 'axios'

import { domain } from './data'

let url = domain

export function addClient (data, callBack, errorCallBack) {
	axios({ url: url + '/api/add/client', data: data,
		headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function getClients (callBack, errorCallBack) {
	axios({ url: url + '/api/get/clients',
		headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'GET' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function getDistriProd (data, callBack, errorCallBack) {
	axios({ url: url + '/api/get/distribuidor/products', data: data,
		headers: { 'Accept': 'application/json;charset=utf-8'} , method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function pdf (data, callBack, errorCallBack) {
	axios({ url: url + '/api/generate/pdf/'+ data.username, method: 'GET' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function deleteClients (data, callBack, errorCallBack) {
	axios({ url: url + '/api/delete/clients', data: data,
	headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function updateClients (data, callBack, errorCallBack) {
	axios({ url: url + '/api/update/clients', data: data,
	headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}
