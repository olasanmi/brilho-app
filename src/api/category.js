import axios from 'axios'

import { domain } from './data'

let url = domain

export function addCategory (data, callBack, errorCallBack) {
	axios({ url: url + '/api/add/category', data: data,
		headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function editCategory (data, callBack, errorCallBack) {
	axios({ url: url + '/api/edit/category', data: data,
		headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function addTheme (data, callBack, errorCallBack) {
	axios({ url: url + '/api/add/themes', data: data,
		headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function editThemes (data, callBack, errorCallBack) {
	axios({ url: url + '/api/edit/themes', data: data,
		headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function getCategory (callBack, errorCallBack) {
	axios({ url: url + '/api/get/category',
		headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'GET' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function deleteCategory (data, callBack, errorCallBack) {
	axios({ url: url + '/api/delete/category', data: data,
	headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function deleteTheme (data, callBack, errorCallBack) {
	axios({ url: url + '/api/delete/themes', data: data,
	headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function getTheme (callBack, errorCallBack) {
	axios({ url: url + '/api/get/themes',
		headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'GET' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}
