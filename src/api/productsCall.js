import axios from 'axios'

import { domain } from './data'

let url = domain

export function addProducts (data, callBack, errorCallBack) {
	axios({ url: url + '/api/add/products', data: data,
		headers: { 'Accept': 'application/json;charset=utf-8', 'Content-Type': 'multipart/form-data' } ,method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function ediImgs (data, callBack, errorCallBack) {
	axios({ url: url + '/api/edi/products-img', data: data,
		headers: { 'Accept': 'application/json;charset=utf-8', 'Content-Type': 'multipart/form-data' } ,method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function deleteProductOrder (data, callBack, errorCallBack) {
	axios({ url: url + '/api/delete/products/order', data: data,
		headers: { 'Accept': 'application/json;charset=utf-8' } ,method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function toggleBagsState (data, callBack, errorCallBack) {
	axios({ url: url + '/api/toggle/products/bags', data: data,
		headers: { 'Accept': 'application/json;charset=utf-8' } ,method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function getProducts (callBack, errorCallBack) {
	axios({ url: url + '/api/get/products',
		headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'GET' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function getProductUser (data, callBack, errorCallBack) {
	axios({ url: url + '/api/get/products/user/' + data.user_id,
		headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'GET' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function deleteProducts (data, callBack, errorCallBack) {
	axios({ url: url + '/api/delete/product', data: data,
	headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function updateProduct (data, callBack, errorCallBack) {
	axios({ url: url + '/api/update/product', data: data,
	headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}
