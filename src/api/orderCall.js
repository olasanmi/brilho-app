import axios from 'axios'

import { domain } from './data'

let url = domain

export function saveOrder (data, callBack, errorCallBack) {
	axios({ url: url + '/api/save/orders', data: data,
		headers: { 'Accept': 'application/json;charset=utf-8' } ,method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function updateOrders (data, callBack, errorCallBack) {
	axios({ url: url + '/api/update/orders', data: data,
		headers: { 'Accept': 'application/json;charset=utf-8' } ,method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function deleteOrder (data, callBack, errorCallBack) {
	axios({ url: url + '/api/delete/orders', data: data,
		headers: { 'Accept': 'application/json;charset=utf-8' } ,method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function updateIsActives (data, callBack, errorCallBack) {
	axios({ url: url + '/api/update/order/active', data: data,
		headers: { 'Accept': 'application/json;charset=utf-8' } ,method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function getOrders (data, callBack, errorCallBack) {
	axios({ url: url + '/api/get/orders/' + data,
		headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'GET' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function getToday (callBack, errorCallBack) {
	axios({ url: url + '/api/get/index/data',
		headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'GET' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function getTodaySell (callBack, errorCallBack) {
	axios({ url: url + '/api/get/index/data/sell',
		headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'GET' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function getDomicilio (callBack, errorCallBack) {
	axios({ url: url + '/api/data/orders',
		headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'GET' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}
