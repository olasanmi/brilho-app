import axios from 'axios'

import { domain } from './data'

let url = domain

export function registerUser (data, callBack, errorCallBack) {
	axios({ url: url + '/api/register', data: { username: data.user.name, dni: data.user.dni, email: data.user.email, phone: data.user.phone, password: data.user.password },
		headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function loginUser (data, callBack, errorCallBack) {
	axios({ url: url + '/api/logins', data: data,
		headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function getRestaurant (callBack, errorCallBack) {
	axios({ url: url + '/api/restaurants', headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'GET' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

//filter
export function getFoodType (callBack, errorCallBack) {
	axios({ url: url + '/api/foodType', headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'GET' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function restaurantByFoodType (data, callBack, errorCallBack) {
	axios({ url: url + '/api/restaurantes/filter', data: { type: data.type, price: data.price, area: data.area }, headers: { 'Accept': 'application/json;charset=utf-8'} , method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function restaurantByArea (data, callBack, errorCallBack) {
	axios({ url: url + '/api/restaurantes/filter/areas', data: { area: data.area, lat: data.lat, lng: data.lng }, headers: { 'Accept': 'application/json;charset=utf-8'} , method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function restaurantByPrice (data, callBack, errorCallBack) {
	axios({ url: url + '/api/restaurantes/filter/price', data: { price: data.price }, headers: { 'Accept': 'application/json;charset=utf-8'} , method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function foodFilter (data, callBack, errorCallBack) {
	axios({ url: url + '/api/food/filter', data: { food: data.food }, headers: { 'Accept': 'application/json;charset=utf-8'} , method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function recoveryChangePassword (data, callBack, errorCallBack) {
	axios({ url: url + '/api/change/password', data: { email: data.user.email }, headers: { 'Accept': 'application/json;charset=utf-8'} , method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function changePasssWord (data, callBack, errorCallBack) {
	axios({ url: url + '/api/set/change/password', data: { password: data.user.password, id: data.user.id }, headers: { 'Accept': 'application/json;charset=utf-8'} , method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function changeImg (data, callBack, errorCallBack) {
	axios({ url: url + '/api/change/brands', data: data,
		headers: { 'Accept': 'application/json;charset=utf-8', 'Content-Type': 'multipart/form-data' } , method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}
