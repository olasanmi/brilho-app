import axios from 'axios'

import { domain } from './data'

let url = domain

export function addProveedor (data, callBack, errorCallBack) {
	axios({ url: url + '/api/add/proveedor', data: data,
		headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function addFactura (data, callBack, errorCallBack) {
	axios({ url: url + '/api/add/factura', data: data,
		headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function deleteProveedors (data, callBack, errorCallBack) {
	axios({ url: url + '/api/delete/proveedor', data: data,
	headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function toggleStatuss (data, callBack, errorCallBack) {
	axios({ url: url + '/api/toggle/status', data: data,
	headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function getProveedor (callBack, errorCallBack) {
	axios({ url: url + '/api/get/proveedor',
		headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'GET' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}
