import axios from 'axios'

import { domain } from './data'

let url = domain

export function addCategory (data, callBack, errorCallBack) {
	axios({ url: url + '/api/add/subcategory', data: data,
		headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function editCategory (data, callBack, errorCallBack) {
	axios({ url: url + '/api/edit/subcategory', data: data,
		headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function getCategory (callBack, errorCallBack) {
	axios({ url: url + '/api/get/subcategory',
		headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'GET' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}

export function deleteCategory (data, callBack, errorCallBack) {
	axios({ url: url + '/api/delete/subcategory', data: data,
	headers: { 'Accept': 'application/json;charset=utf-8'} ,method: 'POST' })
	.then(response => {
		callBack(response)
	})
	.catch(err => {
		if (errorCallBack != null) {
			errorCallBack(err)
		}
	})
}
