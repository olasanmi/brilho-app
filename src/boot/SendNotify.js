import SendNotify from 'src/utils/Notify'
import Vue from 'vue'

export default async () => {
  Vue.prototype.$SendNotify = SendNotify
}
