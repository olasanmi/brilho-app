import RouterPush from 'src/utils/RouterPush'
import Vue from 'vue'

export default () => {
  Vue.prototype.$PushRoute = RouterPush
}
