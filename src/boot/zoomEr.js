import 'viewerjs/dist/viewer.css'
import Viewer from 'v-viewer'
import Vue from 'vue'

export default async () => {
  Vue.use(Viewer)
}
