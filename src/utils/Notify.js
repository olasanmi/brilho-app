import { Notify } from 'quasar'

const sendNotify = function (type, message, color) {
  Notify.create({
    message: message,
    type: type,
    color: color,
    position: 'top-right'
  })
}

export default sendNotify
